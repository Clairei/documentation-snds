# Programmes
<!-- SPDX-License-Identifier: MPL-2.0 -->

Cette page référence des dépôt public de programmes sur le SNDS dont le code est public.


## Dépôt commun

Le dépôt [programmes-snds](https://gitlab.com/healthdatahub/programmes-sdns) héberge des programmes produits par différentes organisations, et partagés sous licence `Apache-2.0`. Il est maintenu par le Health Data Hub.

Le README principal explique l'organisation du dépôt. 
Le document [CONTRIBUTING.md](https://gitlab.com/healthdatahub/programmes-sdns/blob/master/CONTRIBUTING.md) explique les bonnes pratiques pour contribuer à ce dépôt.
